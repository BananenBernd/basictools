from distutils.core import setup

setup(
    name='basictools',
    version='0.1.0',
    author='Karl-L. Besser',
    author_email='karl-ludwig.besser@tu-dresden.de',
    description='Package which provides basic functions for Python',
    long_description=open('README.md').read(),
    install_requires=["numpy"],
)
