import numpy as np


def sort_together(list1, list2):
    return [list(t) for t in zip(*sorted(zip(list1, list2)))]


def as_logic_index_vector(N, idx):
    vec = np.array([False]*N, dtype=bool)
    vec[idx] = True
    return vec
