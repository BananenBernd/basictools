import matplotlib.pyplot as plt


def plot_dict(x, fig=None, **kwargs):
    """Plot a dictonary where the keys are the independent variables and the 
    values the function values.

    Parameters
    ----------
    x : dict
        Dict with the values to plot

    Returns
    -------
    fig : pyplot.Figure
        Figure of the plots
    """
    if fig is None:
        fig = plt.figure()
    fig.plot(x.keys(), x.values(), **kwargs)
    return fig
