from datetime import datetime


EPOCH = datetime.utcfromtimestamp(0)


def time_since_epoch(timestamp):
    """Converts a datetime object into seconds since epoch.

    Parameters
    ----------
    timestamp : datetime
        Datetime object

    Returns
    -------
    seconds_since_epoch : int
        Seconds since epoch (01/01/1970)
    """
    return (timestamp-EPOCH).total_seconds()
