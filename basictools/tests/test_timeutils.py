from datetime import datetime

import pytest

from basictools import timeutils


@pytest.mark.parametrize("date_input, expected",
                         [(datetime(1970, 1, 1, 0, 0), 0),
                          (datetime(2017, 11, 30, 0, 0), 1512000000),
                          (datetime(1960, 12, 24, 0, 0, 2), -284687998)])
def test_time_since(date_input, expected):
    assert timeutils.time_since_epoch(date_input) == expected
