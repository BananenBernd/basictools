import pytest
import numpy as np

from basictools import listutils


@pytest.mark.parametrize("list1, list2, expected",
                         [([3, 2, 5, 1], ['3', '2', '5', '1'],
                           ([1, 2, 3, 5], ['1', '2', '3', '5']))])
def test_sort_together(list1, list2, expected):
    sort1, sort2 = listutils.sort_together(list1, list2)
    assert (sort1, sort2) == expected


@pytest.mark.parametrize("length, idx, expected", 
                         [(3, [0], [True, False, False]),
                          (5, [1, 3], [False, True, False, True, False])])
def test_as_logic_index_vector(length, idx, expected):
    vec = listutils.as_logic_index_vector(length, idx)
    assert all(vec == expected)
